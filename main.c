#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

void randArray(int rows, int column, int rangeBegin, int rangeEnd, int ArrRand[][column])
{
  srand(time(NULL));
  for (int i = 0; i < rows; i++)
  {
    for (int j = 0; j < column; j++)
    {
      ArrRand[i][j] = rand() % ((rangeEnd - rangeBegin + 1) + rangeBegin); // ArrRand[rows][column]
    }
  }

  for (int i = 0; i < rows; i++)
  {
    for (int j = 0; j < column; j++)
    {
      printf("%d ", ArrRand[i][j]);
    }
    printf("\n");
  }
}
void defineArray(int rows, int column, int Arr[][column])
{
  int n;
  for (int i = 0; i < rows; i++)
  {
    for (int j = 0; j < column; j++)
    {
      printf("please input arr: ");
      scanf("%d", &n);
      Arr[i][j] = n;
    }
  }

  for (int i = 0; i < rows; i++)
  {
    for (int j = 0; j < column; j++)
    {
      printf("%d ", Arr[i][j]);
    }
    printf("\n");
  }
}

int compareArray(int rows, int column, int Arr1[][column], int Arr2[][column])
{
  int arrInput1;
  int arrInput2;
  bool same;
  for (int i = 0; i < rows; i++)
  {
    for (int j = 0; j < column; j++)
    {
      printf("input Arr1: ");
      scanf("%d", &arrInput1);
      Arr1[i][j] = arrInput1;
      printf("input Arr2: ");
      scanf("%d", &arrInput2);
      Arr1[i][j] = arrInput2;
    }
  }

  for (int i = 0; i < rows; i++)
  {
    for (int j = 0; j < column; j++)
    {
      if (Arr1[i][j] != Arr2[i][j])
      {
        same = false;
        break;
      }
    }
  }
  if (same)
  {
    printf("same!");
  }
  else
  {
    printf("not same");
  }

  return 0;
}

int SortArray(int sizeOfArray, int Arr[][sizeOfArray])
{
  int temp = 0;
  defineArray(sizeOfArray, sizeOfArray, Arr);

  // sorting left diagnal
  for (int i = 0; i < sizeOfArray; i++)
  {
    for (int j = 0; j < sizeOfArray - i - 1; j++)
    {
      if (Arr[j + 1][j + 1] < Arr[j][j])
      {
        temp = Arr[j][j];
        Arr[j][j] = Arr[j + 1][j + 1];
        Arr[j + 1][j + 1] = temp;
      }
    }
  }

  for (int i = 0; i < sizeOfArray; i++)
  {
    for (int j = 0; j < sizeOfArray - i - 1; j++)
    {
      if (Arr[j + 1][j + 1] < Arr[j][j])
      {
        temp = Arr[j][j];
        Arr[j][j] = Arr[j + 1][j + 1];
        Arr[j + 1][j + 1] = temp;
      }
    }
  }

  for (int i = 0; i < sizeOfArray; i++)
  {
    for (int j = 0; j < sizeOfArray - i - 1; j++)
    {
      if (Arr[sizeOfArray - j - 2][j + 1] < Arr[sizeOfArray - j - 1][j])
      {
        temp = Arr[sizeOfArray - j - 1][j];
        Arr[sizeOfArray - j - 1][j] = Arr[sizeOfArray - j - 2][j + 1];
        Arr[sizeOfArray - j - 2][j + 1] = temp;
      }
    }
  }

  printf("sorted\n");
  for (int i = 0; i < sizeOfArray; i++)
  {
    for (int j = 0; j < sizeOfArray; j++)
    {
      printf("%d ", Arr[i][j]);
    }
    printf("\n");
  }
  return 0;
}

void Transpose(int sizeArr, int Arr1[][sizeArr], int Arr2[][sizeArr])
{
  for (int i = 0; i < sizeArr; i++)
  {
    for (int j = 0; j < sizeArr; j++)
    {
      Arr2[i][j] = Arr1[j][i];
    }
  }

  printf("\n");
  for (int i = 0; i < sizeArr; i++)
  {
    for (int j = 0; j < sizeArr; j++)
    {
      printf("%d ", Arr2[i][j]);
    }
    printf("\n");
  }
}

void SymatricMainDiagnol(int sizeOfArray, int Arr[][sizeOfArray])
{
  defineArray(sizeOfArray, sizeOfArray, Arr);

  int isSymetric = 0;
  for (int i = 0; i < sizeOfArray; i++)
  {
    for (int j = 0; j < sizeOfArray; j++)
    {
      if (Arr[j][i] != Arr[i][j])
      {
        isSymetric = 1;
        break;
      }
    }
  }

  if (isSymetric == 0)
  {
    printf("Symatric!");
  }
  else
  {
    printf("not Symatric");
  }
}

void isSymetricAntiDiagnol(int sizeOfArray, int Arr[][sizeOfArray])
{
  int temp;
  defineArray(sizeOfArray, sizeOfArray, Arr);
  for (int i = 0; i < sizeOfArray; i++)
  {
    for (int j = 0; j < sizeOfArray / 2; j++)
    {
      temp = Arr[i][j];
      Arr[i][j] = Arr[i][sizeOfArray - 1 - j];
      Arr[i][sizeOfArray - 1 - j] = temp;
    }
  }
  printf("\n");
  for (int i = 0; i < 3; i++)
  {
    for (int j = 0; j < 3; j++)
    {
      printf("%d ", Arr[i][j]);
    }
    printf("\n");
  }

  int isSymetric = 0;
  for (int i = 0; i < sizeOfArray; i++)
  {
    for (int j = 0; j < sizeOfArray; j++)
    {
      if (Arr[j][i] != Arr[i][j])
      {
        isSymetric = 1;
        break;
      }
    }
  }

  if (isSymetric == 0)
  {
    printf("Symatric!");
  }
  else
  {
    printf("not Symatric");
  }
}

int main()
{
  int Arr[3][3];
  isSymetricAntiDiagnol(3, Arr);
}

/*bunch of code can be rewritten into function but too late to rewrite them*/